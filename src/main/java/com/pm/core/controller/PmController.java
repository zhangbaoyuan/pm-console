package com.pm.core.controller;

import com.pm.core.api.PmApi;
import com.pm.core.pojo.dto.PmDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class PmController {

    @Resource
    private PmApi pmApi;

    @RequestMapping("/pmList")
    @ResponseBody
    public List<PmDto> pmList(){
        List<PmDto> pmDtoList = pmApi.pmList();
        return pmDtoList;
    }
}
